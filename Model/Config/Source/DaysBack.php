<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ImportOrdersFromAmazon\Model\Config\Source;


class DaysBack implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('1 Day')],
            ['value' => '2', 'label' => __('2 Days')],
            ['value' => '3', 'label' => __('3 Days')],
            ['value' => '4', 'label' => __('4 Days')],
            ['value' => '5', 'label' => __('5 Days')],
            ['value' => '6', 'label' => __('6 Days')],
            ['value' => '7', 'label' => __('7 Days')],
            ['value' => '14', 'label' => __('14 Days')],
            ['value' => '30', 'label' => __('30 Days')]
            ];
    }

    public function toArray()
    {
        $options = $this->toOptionArray();
        $arr = [];
        foreach ($options as $value => $label){
            $arr[$value] = $label;
        }
        return $arr;
    }
}

