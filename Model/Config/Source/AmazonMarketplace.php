<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ImportOrdersFromAmazon\Model\Config\Source;

use AmazonPHP\SellingPartner\Marketplace;

class AmazonMarketplace implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => Marketplace::CA()->id(), 'label' => Marketplace::CA()->name()],
            ['value' => Marketplace::US()->id(), 'label' => Marketplace::US()->name()],
            ['value' => Marketplace::MX()->id(), 'label' => Marketplace::MX()->name()],
            ['value' => Marketplace::BR()->id(), 'label' => Marketplace::BR()->name()],
            ['value' => Marketplace::ES()->id(), 'label' => Marketplace::ES()->name()],
            ['value' => Marketplace::GB()->id(), 'label' => Marketplace::GB()->name()],
            ['value' => Marketplace::FR()->id(), 'label' => Marketplace::FR()->name()],
            ['value' => Marketplace::BE()->id(), 'label' => Marketplace::BE()->name()],
            ['value' => Marketplace::NL()->id(), 'label' => Marketplace::NL()->name()],
            ['value' => Marketplace::DE()->id(), 'label' => Marketplace::DE()->name()],
            ['value' => Marketplace::IT()->id(), 'label' => Marketplace::IT()->name()],
            ['value' => Marketplace::SE()->id(), 'label' => Marketplace::SE()->name()],
            ['value' => Marketplace::PL()->id(), 'label' => Marketplace::PL()->name()],
            ['value' => Marketplace::EG()->id(), 'label' => Marketplace::EG()->name()],
            ['value' => Marketplace::TR()->id(), 'label' => Marketplace::TR()->name()],
            ['value' => Marketplace::SA()->id(), 'label' => Marketplace::SA()->name()],
            ['value' => Marketplace::AE()->id(), 'label' => Marketplace::AE()->name()],
            ['value' => Marketplace::IN()->id(), 'label' => Marketplace::IN()->name()],
            ['value' => Marketplace::SG()->id(), 'label' => Marketplace::SG()->name()],
            ['value' => Marketplace::AU()->id(), 'label' => Marketplace::AU()->name()],
            ['value' => Marketplace::JP()->id(), 'label' => Marketplace::JP()->name()]
        ];
    }

    public function toArray()
    {
        $options = $this->toOptionArray();
        $arr = [];
        foreach ($options as $value => $label) {
            $arr[$value] = $label;
        }
        return $arr;
    }
}

