# Mage2 Module Kowal ImportOrdersFromAmazon

    ``kowal/module-importordersfromamazon``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Import Orders From Amazon

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_ImportOrdersFromAmazon`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-importordersfromamazon`
 - enable the module by running `php bin/magento module:enable Kowal_ImportOrdersFromAmazon`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (importamazon/settings/enable)

 - amazon_marketplace (importamazon/settings/amazon_marketplace)

 - amazon_service_url (importamazon/settings/amazon_service_url)

 - amazon_merchant_id (importamazon/settings/amazon_merchant_id)

 - amazon_access_key_id (importamazon/settings/amazon_access_key_id)

 - amazon_secret_access_key (importamazon/settings/amazon_secret_access_key)


## Specifications

 - Console Command
	- Import

 - Helper
	- Kowal\ImportOrdersFromAmazon\Helper\Config


## Attributes



