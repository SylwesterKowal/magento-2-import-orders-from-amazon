<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ImportOrdersFromAmazon\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        parent::__construct($context);
    }


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->scopeConfig->isSetFlag('importamazon/settings/enable');
    }

    public function getAmazonMarketplace(): array
    {
        $value = $this->scopeConfig->getValue('importamazon/settings/amazon_marketplace');
        $marketlaces = is_null($value) || '' === $value ? '' : $value;
        return explode(",", $marketlaces);
    }

    public function getDaysBack(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/settings/days_back');
        return is_null($value) || '' === $value ? '1' : $value;
    }

    public function getLwaClientID(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/access_details/lwa_client_id');
        return is_null($value) || '' === $value ? '' : $value;
    }

    public function getLwaClientIdSecret(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/access_details/lwa_client_id_secret');
        return is_null($value) || '' === $value ? '' : $value;
    }

    public function getAwsAccessKey(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/access_details/aws_access_key');
        return is_null($value) || '' === $value ? '' : $value;
    }

    public function getAwsSecretKey(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/access_details/aws_secret_key');
        return is_null($value) || '' === $value ? '' : $value;
    }

    public function getRefreshToken(): string
    {
        $value = $this->scopeConfig->getValue('importamazon/access_details/refresh_token');
        return is_null($value) || '' === $value ? '' : $value;
    }

}

