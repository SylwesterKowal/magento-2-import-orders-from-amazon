<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ImportOrdersFromAmazon\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AmazonPHP\SellingPartner\Marketplace;
use AmazonPHP\SellingPartner\Regions;
use AmazonPHP\SellingPartner\SellingPartnerSDK;
use Buzz\Client\Curl;
use AmazonPHP\SellingPartner\Exception\ApiException;
use AmazonPHP\SellingPartner\Configuration;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Nyholm\Psr7\Factory\Psr17Factory;

class Import extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\ImportOrdersFromAmazon\Helper\Config $config
    )
    {

        $this->config = $config;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("START " . date('Y-m-d H:i:s'));


        $factory = new Psr17Factory();
        $client = new Curl($factory);

        $configuration = Configuration::forIAMUser(
            $this->config->getLwaClientID(),
            $this->config->getLwaClientIdSecret(),
            $this->config->getAwsAccessKey(),
            $this->config->getAwsSecretKey()
        );

        $logger = new Logger('amazon_sp_api');
        $logger->pushHandler(new StreamHandler(__DIR__ . '/sp-api-php.log', Logger::DEBUG));

        $sdk = SellingPartnerSDK::create($client, $factory, $factory, $configuration, $logger);

        $accessToken = $sdk->oAuth()->exchangeRefreshToken($this->config->getRefreshToken());

        $orders = [];
        try {
            $orders = $sdk->orders()->getOrders(
                $accessToken,
                Regions::EUROPE,
                $marketplaceId = $this->config->getAmazonMarketplace(),
                date('Y-m-d', strtotime("-{$this->config->getDaysBack()} days"))
            );
            foreach ($orders as $order) {
                echo print_r($order, true);
            }
        } catch (ApiException $exception) {
            dump($exception->getMessage());
        }

        $output->writeln("END " . date('Y-m-d H:i:s') . ' / ' . count($orders) . ' orders imported from Amazon to Magento');

        return 1;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_importordersfromamazon:import");
        $this->setDescription("Import Orders from Amazon");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

